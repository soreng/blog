'use strict';

/**
 * Module dependencies.
 */
var articlesPolicy = require('../policies/articles.server.policy'),
  articles = require('../controllers/articles.server.controller');

module.exports = function (app) {
  app.route('/api/createrss').post(articles.createRss);
  app.route('rss').get(articles.readRss);
  // Articles collection routes
  app.route('/api/articles').all(articlesPolicy.isAllowed)
    .get(articles.list)
    .post(articles.create);

  app.route('/api/posts').all(articlesPolicy.isAllowed)
    .get(articles.listhome);

  // Single article routes
  app.route('/api/articles/:slug').all(articlesPolicy.isAllowed)
    .get(articles.read)
    .put(articles.update)
    .delete(articles.delete);

  // Finish by binding the article middleware
  app.param('slug', articles.articleBySlug);
};
