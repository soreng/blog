'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  fs = require('fs'),
  rss = require('rss'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a article
 */
exports.create = function (req, res) {
  var article = new Article(req.body);
  article.user = req.user;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  res.json(req.article);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
  var article = req.article;

  article.title = req.body.title;
  article.content = req.body.content;
  article.subtitle = req.body.subtitle;
  article.published = req.body.published;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var article = req.article;

  article.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Article.find().sort('-created').populate('user', 'displayName').exec(function (err, articles) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(articles);
    }
  });
};

/**
 * List of Articles
 */
exports.listhome = function (req, res) {
  Article.find({ published: true }).sort('-created').populate('user', 'displayName').exec(function (err, articles) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(articles);
    }
  });
};

/**
 * Article middleware
 */
exports.articleBySlug = function (req, res, next, id) {

  /*
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Article is invalid'
    });
  }
  */

  Article.findById(id).populate('user', 'displayName').exec(function (err, article) {
    if (err) {
      return next(err);
    } else if (!article) {
      return res.status(404).send({
        message: 'No article with that identifier has been found'
      });
    }
    req.article = article;
    next();
  });
};

exports.readRss = function(req, res) {
  fs.readFile(config.rssfeed, function (err, data) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    res.set('Content-Type', 'application/rss+xml');
    res.send(data);
  });
  
};

exports.createRss = function (req, res) {
  var feed = new rss({
    title: 'TheScrumBlog.com',
    description: 'For the most part a blog about Scrum',
    feed_url: 'http://thescrumblog.com/rss.xml',
    site_url: 'http://thescrumblog.com',
    managingEditor: 'Stephan Kristiansen',
    webMaster: 'Stephan Kristiansen',
    copyright: 'Copyright © 2015 Stephan Kristiansen. All rights reserved',
    language: 'en'
  });

  Article.find().sort('-created').populate('user', 'displayName').exec(function (err, articles) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      for(var key in articles) {
        feed.item({
          title:          articles[key].title,
          url:            'http://thescrumblog.com/posts/' + articles[key].slug,
          description:    articles[key].subtitle,
          date:           articles[key].created,
          author:         articles[key].displayName
        });
      }

      var xml = feed.xml({ indent: true });

      fs.writeFile(config.rssfeed, xml, function(err) {
        if(err) {
          return console.log(err);
        }
        res.status(200).send({
          message: 'feed generated!',
          feed: feed
        });
      }); 
    }
  }); 
};
