'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  friendly = require('mongoose-friendly'),
  Schema = mongoose.Schema;


/**
 * Article Schema
 */
var ArticleSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  subtitle: {
    type: String,
    default: '',
    trim: true,
  },
  content: {
    type: String,
    default: '',
    trim: true
  },
  published: {
    type: Boolean,
    default: false
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  category: {
    type: String
  }
});

ArticleSchema.plugin(friendly, {
  source: 'title',  // Attribute to generate the friendly version from. 
  friendly: 'slug', // Attribute to set the friendly version of source. 
  addIndex: true,   // Sets {unique: true} as index for the friendly attribute. 
  findById: true    // Turns findById into an alias for findByFriendly. 
});

mongoose.model('Article', ArticleSchema);
