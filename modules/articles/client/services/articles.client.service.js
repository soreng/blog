'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', ['$resource',
  function ($resource) {
    return $resource('api/articles/:slug', {
      slug: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
])
.factory('Posts', ['$resource',
  function ($resource) {
    return $resource('api/posts/');
  }
]);
