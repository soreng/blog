'use strict';

// Articles controller
angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Articles',
  function ($scope, $stateParams, $location, $http, Authentication, Articles) {
    $scope.authentication = Authentication;

    $scope.tinymceOptions = {
      selector: 'textarea',
      onChange: function(e) {
        // put logic here for keypress and cut/paste changes
      },
      inline: false,
      plugins : 'advlist autolink link image lists charmap print preview code',
      skin: 'lightgray',
      theme : 'modern'
    };
    // Create new Article
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }

      // Create new Article object
      var article = new Articles({
        title: this.title,
        subtitle: this.subtitle,
        content: this.content,
        published: this.published
      });

      // Redirect after save
      article.$save(function (response) {
        $location.path('articles/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (article) {
      if (article) {
        article.$remove();

        for (var i in $scope.articles) {
          if ($scope.articles[i] === article) {
            $scope.articles.splice(i, 1);
          }
        }
      } else {
        $scope.article.$remove(function () {
          $location.path('articles');
        });
      }
    };

    // Update existing Article
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }

      var article = $scope.article;

      article.$update(function () {
        $location.path('articles/' + article.slug);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.articles = Articles.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.article = Articles.get({
        slug: $stateParams.slug
      });
    };

    $scope.createRss = function () {
      $http.post('/api/createrss').success(function(data, status) {
        console.log('rss feed generated, ' + data.message + ', feed: ' + data.feed);
      });
    };
  }
]);
