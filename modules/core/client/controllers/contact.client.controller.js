'use strict';
var ModalInstanceCtrl = function ($scope, $modalInstance, $window) {
  $scope.ok = function () {
    $window.location.href = '/';
  };
};

angular.module('core').controller('ContactController', ['$scope', '$http', '$location', '$modal', '$window',
function($scope, $http, $location, $modal, $window) {

  // Get an eventual error defined in the URL query string:
  $scope.error = $location.search().err;
  // the naming of our data model is consistent
  $scope.contact = {};
  $scope.sent = false;
  $scope.sentSuccess = '';

  // when the user submits it triggers processForm()
  $scope.processForm = function(isValid) {
    $scope.error = null;
    $scope.sentSuccess = '';
    if (!isValid) {
      $scope.$broadcast('show-errors-check-validity', 'contactForm');
      return false;
    }
    $http.post('/sendContact', $scope.contact)
      .success(function(response) {
      //console.log('Success! :D');     
      }).error(function(response) {
        console.log(response);
      });


    var modalInstance = $modal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'sentmessagedialog.html',
      controller: ModalInstanceCtrl,
      size: 'sm'
    });

    $scope.contact = {};
    $scope.sent = true;
    $scope.sentSuccess = 'Message sent successfully! I will get back to you ASAP.';
  };
}]);