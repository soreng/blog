'use strict';

// Articles controller
angular.module('articles').controller('PostController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Articles', 'Posts',
  function ($scope, $http, $stateParams, $location, Authentication, Articles, Posts) {
    $scope.authentication = Authentication;
    $scope.feedLimit = 5;
    // Find a list of Articles
    $scope.find = function () {
      //$scope.articles = Articles.query();

      $scope.articles = Posts.query();
    };


    // Find existing Article
    $scope.findOne = function () {
      $scope.article = Articles.get({
        slug: $stateParams.slug
      });
    };
  }
]);
