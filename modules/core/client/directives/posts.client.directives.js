'use strict';

angular.module('core').directive('dynFbCommentBox', function () {
  function createHTML(href, numposts, colorscheme, orderby) {
    return '<div class="fb-comments" ' +
                   'data-href="' + href + '" ' +
                   'data-numposts="' + numposts + '" ' +
                   'data-order-by="' + orderby + '" ' +
                   'data-colorsheme="' + colorscheme + '">' +
           '</div>';
  }

  return {
    restrict: 'A',
    scope: {},
    link: function postLink(scope, elem, attrs) {
      attrs.$observe('pageHref', function (newValue) {
        var href = newValue;
        var numposts = attrs.numposts || 5;
        var colorscheme = attrs.colorscheme || 'light';
        var orderby = attrs.orderby || 'time';

        elem.html(createHTML(href, numposts, colorscheme, orderby));
        FB.XFBML.parse(elem[0]);
      });
    }
  };
});