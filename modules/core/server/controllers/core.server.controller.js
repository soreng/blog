'use strict';
var nodemailer = require('nodemailer'),
  path = require('path'),
  config = require(path.resolve('./config/config')),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  fs = require('fs'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {
  res.render('modules/core/server/views/index', {
    user: req.user || null
  });
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};

var transport = nodemailer.createTransport('SMTP', {
  service: 'gmail',
  auth: {
    user: config.mailer.user,
    pass: config.mailer.pass
  }
});

exports.sendContact = function(req, res) {

  // logs to grunt task
  console.log('sent inquiry');
  
  var mailOptions = {
    to: 'thescrumblog@gmail.com',
    from: 'thescrumblog@gmail.com',
    subject: 'Inquiry from: [' + req.body.name + ']',
    html: 'From: ' + req.body.name + '<br/>' +
          'Email: ' + req.body.email + '<br/>' +
          'Phone: ' + req.body.phone + '<br/>' +
          'Message: ' + req.body.message + '<br/>'
  };

  transport.sendMail(mailOptions, function(error, response){
    if(error){
      console.log('Mail send error: ' + error);
    }else{
      console.log('Message sent: ' + response.message);
    }
  });
};

exports.getImageList = function(req, res){
  var result = [];
  fs.readdir(config.uploads.imageUpload.dest, function(err, files) {
    if (err) return;
    files.forEach(function(file) {
      result.push({ filename: file.name });
    });
  });
  res.json(result);
};

/**
 * Show the current article
 */
exports.readArticle = function (req, res) {
  res.json(req.article);
};