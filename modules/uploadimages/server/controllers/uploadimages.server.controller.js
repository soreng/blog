'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  multer = require('multer'),
  config = require(path.resolve('./config/config')),
  mime = require('mime'),
  User = mongoose.model('User');

/**
 * Upload images
 */
exports.uploadImages = function (req, res) {
  var user = req.user;
  var message = null;

  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, config.uploads.imageUpload.dest);
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname + '-' + Date.now() + '.' + mime.extension(file.mimetype));
    }
  });
  var upload = multer({ storage: storage, limits: config.uploads.imageUpload.limits }).array('upl');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;
  
  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (user) {
    upload(req, res, function (uploadError) {
      if(uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading image'
        });
      } else {
        return res.status(200).send({
          message: 'Image successfully uploaded!'
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};
