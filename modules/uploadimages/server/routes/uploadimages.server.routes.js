'use strict';

/**
 * Module dependencies.
 */
var imageUpload = require('../controllers/uploadimages.server.controller');
module.exports = function (app) {
  app.route('/api/uploadimages').post(imageUpload.uploadImages); 
};
