'use strict';

angular.module('chat').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('uploadimages', {
        url: '/uploadimages',
        templateUrl: 'modules/uploadimages/client/views/uploadimages.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      });
  }
]);
