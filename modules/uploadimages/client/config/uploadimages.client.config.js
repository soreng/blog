'use strict';

angular.module('uploadimages').run(['Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Upload images',
      state: 'uploadimages'
    });
  }
]);

